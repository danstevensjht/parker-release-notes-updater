/// <reference path="./lib.deno_runtime.d.ts"/>

import readerToString from './reader_to_string.ts';
import stringToWriter from './string_to_writer.ts';


function htmlEscapeText(text:string):string {
	return text.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g,'&quot;')
}

function htmlLink(targetUrl:string, linkText:string):string {
	return `<a href="${htmlEscapeText(targetUrl)}">${htmlEscapeText(linkText)}</a>`;
}

function prksLink(ticketId:string):string {
	return htmlLink(`https://jhtnaee.atlassian.net/browse/${ticketId}`, ticketId);
}

function bitbucketCommutLink(commitHash:string):string {
	return htmlLink(`https://bitbucket.org/johnsonhealthtech/chimera2/commits/${commitHash}`, commitHash);
}


function htmlEscapeAndEnhanceText(text:string):string {
	return htmlEscapeText(text)
		.replace(/PRKS-\d+/g, prksLink)
		.replace(/\b[0-9a-f]{40}\b/, bitbucketCommutLink);
}

interface LineReader {
	readLine():Promise<string|null>;
}

class StringLineReader implements LineReader {
	protected lineNumber = 0;
	constructor(protected lines:string[]) {}
	readLine():Promise<string|null> {
		return Promise.resolve(this.lineNumber > this.lines.length ? null : this.lines[this.lineNumber++]);
	}
}

interface VersionInfo {
	name? : string;
	date? : string;
	changesByCategory : {[categoryName:string]: string[]}
}
interface Changelog {
	versions : VersionInfo[];
}

function versionIsEmpty(version:VersionInfo):boolean {
	if( version.name != undefined ) return false;
	if( version.date != undefined ) return false;
	for( let c in version.changesByCategory) return false;
	return true;
}

function objectIsEmpty(obj:any):boolean {
	for( let k in obj ) if( obj.hasOwnProperty(k) ) return false;
	return true;
}

async function parseChangelog(lineReader:LineReader):Promise<Changelog> {
	let changelog:Changelog = { versions: [] };
	let version : VersionInfo = { changesByCategory: {} };
	let changeList : string[]|undefined = undefined; // for current category of current version
	let m:RegExpExecArray|null;
	while( true ) {
		let line = await lineReader.readLine();
		if( line == "" ) {
		} else if( line == null || line.startsWith("---") ) {
			if( !versionIsEmpty(version) ) changelog.versions.push(version);
			version = { changesByCategory: {} };
			changeList = undefined;
			if( line == null ) break;
		} else if( (m = /^([^\s:]+):\s+(.*)/.exec(line)) ) {
			// You can put any metadata about the version in there you want!
			// But the keys will be lowercased.
			// Suggested coding for multi-word keys is "with-dashes".
			let k = m[1].toLowerCase();
			if( k == 'version' ) k = 'name'; // Special case because 'version.version' is dumb!
			if( k == "changesByCategory" ) throw new Error("Oops, reserved version metadata field '"+k+"' used");
			(version as any)[k] = m[2];
		} else if( (m = /^  (\S.*):$/.exec(line)) ) {
			changeList = version.changesByCategory[m[1]] = [];
		} else if( (m = /^    - (.*)$/.exec(line)) ) {
			if( changeList == undefined ) throw new Error("Change item occured not in a category!");
			changeList.push(m[1]);
		} else if( (m = /^      (.*)$/.exec(line)) ) {
			if( changeList == undefined ) throw new Error("Change item continuation occured not in a category!");
			if( changeList.length == 0 ) throw new Error("Change item continuation occured without a first line!");
			changeList[changeList.length-1] += "\n" + m[1];
		} else {
			throw new Error("Bad line in changelog: "+line);
		}
	}
	return changelog;
}


const unreleasedRegex = /^\?+$/

readerToString(Deno.stdin).then( changelogTxt => {
	return new StringLineReader(changelogTxt.split("\n"));
}).then( parseChangelog ).then( (changelog:Changelog) => {

	// For now we do it easy and not super pretty!
	let htmlLines:string[] = [
		'<p>This page tracks changes to com.jht.chimera (parker flavor) apk.',
		'It is generated automatically from parker2-changelog.txt.  Do not edit this page directly.</p>',
		'<ac:structured-macro ac:name="note" ac:schema-version="1" ac:macro-id="0aef2db1-a795-43c2-896e-b082df125d68"><ac:rich-text-body><p><strong>Do not update the console if you have a REV B or earlier console!</strong> The REV C has hardware changes to define the screen resolution which the OS 1.0.0.10 and above requires. You will need to rename the update.xml.no_os to update.xml and use this file.</p></ac:rich-text-body></ac:structured-macro>',
		'<ac:structured-macro ac:name="info" ac:schema-version="1" ac:macro-id="bdc16af5-f119-4461-882b-fdac48e6174c"><ac:rich-text-body><p><strong>Note:</strong> Only the frame type currently defined in the following link work with RSCU</p><p><ac:link ac:card-appearance="inline"><ri:page ri:space-key="PP" ri:content-title="Software Notes" ri:version-at-save="5" /><ac:link-body>Software Notes</ac:link-body></ac:link> </p></ac:rich-text-body></ac:structured-macro>',
	];
		//"<pre>" + htmlEscapeText(changelogTxt) + "</pre>\n";

	for( let v in changelog.versions ) {
		let version = changelog.versions[v];
		let title = (version.name != undefined && !unreleasedRegex.test(version.name)) ? version.name : "(unreleased)";
		if( version.date != undefined && !unreleasedRegex.test(version.date) ) title += " ("+version.date+")";
		htmlLines.push('<ac:structured-macro ac:name="expand" ac:schema-version="1">');
		htmlLines.push('\t<ac:parameter ac:name="title">'+htmlEscapeAndEnhanceText(title)+"</ac:parameter>");
		htmlLines.push('\t<ac:rich-text-body><ul>');
		for( let cc in version.changesByCategory ) {
			let changes = version.changesByCategory[cc];
			if( objectIsEmpty(changes) ) continue;
			htmlLines.push('\t\t<li>'+htmlEscapeAndEnhanceText(cc)+'<ul>');
			for( let ci in changes ) {
				htmlLines.push('\t\t\t<li>'+htmlEscapeAndEnhanceText(changes[ci])+'</li>');
			}
			htmlLines.push('\t\t</ul></li>');
		}
		htmlLines.push('\t</ul></ac:rich-text-body>');
		htmlLines.push('</ac:structured-macro>');
	}

	return stringToWriter(htmlLines.join("\n")+"\n", Deno.stdout);
}).then( () => {
	return 0;
}).catch( e => {
	console.error(e.stack);
	return 1;
}).then( (code:number) => {
	Deno.exit(code);
})
