@echo off

rem These scripts are written in TypeScript and run with Deno (see https://deno.land/)
rem because:
rem A) I like TypeScript
rem B) I wanted to try out Deno
rem C) I figured it would be quicker (or at least less agonizing)
rem    to do the JSON and HTTP stuff in TypeScript than in Java
rem D) Java would have required a build process, more JAR files, etc.
rem    Deno is simple to run in comparison; you just need deno.exe and a script.
rem 
rem Sorry if adding yet another language made the codebase worse lolol.
rem But it should be easy to port this all to Java or Kotlin if we ever need to.
rem --Dan
rem 
rem Anyway, usage instructions:
rem - set chimera2_dir, content_url, page_title variables (see .env.bat.example)
rem - set auth_base64=(the base64 of "${username}:${auth token}")
rem   - I have a .gitignored bat file that does this for me
rem - set deno_exe to the path of deno.exe, if not in %Path%
rem - run this script
rem - profit!

if not defined deno_exe set deno_exe=deno.exe
if not defined chimera2_dir (echo chimera2_dir not set >&2 & goto fail)

rem "%deno_exe%" run %~dp0/generate_release_notes_html.ts <%chimera2_dir%/parker2-changelog.txt | "%deno_exe%" run --allow-net --allow-env %~dp0/update_confluence_page.ts

"%deno_exe%" run %~dp0/generate_release_notes_html.ts <%chimera2_dir%/parker2-changelog.txt >release-notes.html
"%deno_exe%" run --allow-net --allow-env %~dp0/update_confluence_page.ts <release-notes.html

goto eof
:fail
echo %~n0 exiting with errorlevel 1 due to errors >&2
exit /B 1
:eof
