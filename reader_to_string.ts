export default async function readerToString(stream:Deno.Reader):Promise<string> {
	let buffer = new Uint8Array(1024*1024);
	let position:number = 0;
	while( true ) {
		let read:number|null = await stream.read(buffer.subarray(position));
		if( read == null ) break;
		if( buffer.length == position ) {
			throw new Error("Filled entire "+buffer.length+" buffer while reading stream!")
		}
		position += read;
	}
	//console.log("Read "+position+" bytes");
	let utf8Decoder = new TextDecoder("utf-8");
	return utf8Decoder.decode(buffer.slice(0, position));
}
