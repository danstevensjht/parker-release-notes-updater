/// <reference path="./lib.deno_runtime.d.ts"/>

interface PublishParameters {
	contentUrl : string;
	authBase64 : string;
	pageTitle  : string;
}

interface Result {
	status: string;
	message?: string;
	code: number;
}

import readerToString from './reader_to_string.ts';

function loadPublishParameters():Promise<PublishParameters> {
	let contentUrl = Deno.env.get("content_url");
	let authBase64 = Deno.env.get("auth_base64");
	let pageTitle  = Deno.env.get("page_title");
	let errors = [];
	if (contentUrl == undefined) errors.push("content_url not specified");
	if (authBase64 == undefined) errors.push("auth_base64 not specified");
	if (pageTitle == undefined) errors.push("page_title not specified");

	if (errors.length == 0) return Promise.resolve({
		contentUrl : contentUrl!,
		authBase64 : authBase64!,
		pageTitle  : pageTitle!,
	});
	return Promise.reject(new Error(errors.join(", ")));
}

let paramsPromise = loadPublishParameters();
let currentContentPromise = paramsPromise.then(parameters => fetch(parameters.contentUrl+"?expand=version,body.storage", {
	headers: new Headers({
		"Authorization": "Basic "+parameters.authBase64
	})
})).then( (res:Response) => {
	if (!res.ok) return Promise.reject(new Error("GET failed with status '"+res.status+" "+res.statusText+"'"))
	return res.json();
});

let newBodyPromise = readerToString(Deno.stdin);

let publishParameters = await paramsPromise;
let newBody = await newBodyPromise;

function minifyHtml(html:string):string {
	return html
		.replace(/ac:macro-id="[^"]+"/g,'') // Confluence might fix our IDs for us; ignore that.
		.replace(/\s*>\s*/g, '>')
		.replace(/\s*<\s*/g, '<')
		.replace(/[\s]+/g,' ')
		.trim();
}

currentContentPromise.then( (currentPageInfo):Promise<Result> => {

	//let newBody = "<p>Hello, World!</p>\n\n<p>Welcome to the release notes.</p>\n\n<ul><li>Version etch<ul><li>Bugfixes:<ul><li>Fixed some stuff</li></ul></li></ul></li></ul>";
	newBody = newBody.trim(); // Confluence seems to remove trailing newlines, so we will, also.

	let newVersion:number = +currentPageInfo.version.number + 1;
	let oldBody = currentPageInfo.body.storage.value;
	if( minifyHtml(newBody) == minifyHtml(oldBody) ) return Promise.resolve({
		status: "unchanged",
		code: 0
	});	

	//console.log( JSON.stringify(oldBody)+" != "+JSON.stringify(newBody) )
	//console.log( "Content changed; uploading..." );

	let newPageInfo = {
		"type": "page",
		"version": {
			"number": newVersion
		},
		"title": publishParameters.pageTitle,
		"body": {
			"storage": {
				"value": newBody,
				"representation": "storage"
			}
		}
	}

	let uploadContent = JSON.stringify(newPageInfo, null, "\t");
	//console.log("Uploading this text:\n"+uploadContent);

	return fetch(publishParameters.contentUrl, {
		method: "PUT",
		headers: {
			"Authorization": "Basic "+publishParameters.authBase64,
			"Content-Type": "application/json",
		},
		body: uploadContent
	}).then( res => {
		if (res.ok) {
			return {
				status: "uploaded",
				code: 0
			}
		} else {
			return res.text().then(responseText => ({
				status: "error",
				message: res.status+" "+res.statusText+"\n\n"+responseText,
				code: 1,
			}));
		}
		return Promise.reject(new Error("POST failed with status '"+res.status+" "+res.statusText+"'"))
	});
}).catch( e => {
	return {
		status: "error",
		message: e.stack,
		code: 1
	}
}).then( result => {
	console.log(result.status)
	if (result.message) console.log(result.message);
	Deno.exit(result.code)
});
