# Parker Release Notes Updater

This is a set of TypeScript scripts, intended to be run using Deno,
that will pull the latest version of the ```parker2``` branch
of the Chimera2 repository,
parse ```parker2-changelog.txt```, generate a release notes page for Confluence,
and post it if it differs from the release notes already there.

To run, you will need:

- A directory containing a checkout of the Chimera2 repository
- Deno (see [link below](#tested-with-deno-v1.0.0) for for a self-contained .exe)
- To set up some environment variables (see [.env.example](./.env.example) or [.env.bat.example](./.env.bat.example))

Entrypoint for that whole process is the shell script, [update](./update),
or, for Windows users, [publish_parker_release_notes.bat](./publish_parker_release_notes.bat)
(which, unlike ```update``` won't try to update the local repo first).

To just generate the release notes HTML without publishing, run:

```sh
deno run generate_release_notes_html.ts <${chimera2_dir}/parker2-changelog.txt
```

(on Windows, replace ```${chimera2_dir}``` with ```%chimera2_dir%```).

## Tested with Deno v1.0.0

[deno.exe for Windows x64](http://wherever-files.nuke24.net/uri-res/raw/urn:bitprint:B2XZ5OK2VM4SZUWCFARJW654DC3ANJQU.FA453FD34EQFHK3M3KJRS73BZZUKKNNP33WQ7TY/deno.exe).

For testing on windows, you can just download deno.exe into this directory.

```
deno 1.0.0
v8 8.4.300
typescript 3.9.2
```

## TODO

- See if it would work with a shallow checkout
