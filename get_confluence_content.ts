/// <reference path="./lib.deno_runtime.d.ts"/>

interface GetParams {
	contentUrl : string;
	authBase64 : string;
}

function loadGetParams():Promise<GetParams> {

	let contentUrl:string|undefined = undefined;
	let authBase64 = Deno.env.get("auth_base64");
	let errors = [];

	let contentUrlSpecifiedByArg;
	for (let a in Deno.args) {
		let arg = Deno.args[a];
		if( /^-/.exec(arg) ) {
			errors.push("Unrecognized option: "+arg);
		} else if( !contentUrlSpecifiedByArg ) {
			contentUrl = arg;
			contentUrlSpecifiedByArg = true;
		} else {
			errors.push("Multiple bare arguments on command-line");
		}
	}

	if (contentUrl == undefined) contentUrl = Deno.env.get("content_url");;

	if (contentUrl == undefined) errors.push("content_url not specified");
	if (authBase64 == undefined) errors.push("auth_base64 not specified");

	if (errors.length == 0) return Promise.resolve({
		contentUrl : contentUrl!,
		authBase64 : authBase64!,
	});
	return Promise.reject(new Error(errors.join(", ")));
}

loadGetParams().then(parameters => fetch(parameters.contentUrl+"?expand=version,body.storage", {
	headers: new Headers({
		"Authorization": "Basic "+parameters.authBase64
	})
})).then( (res:Response) => {
	if (!res.ok) return Promise.reject(new Error("GET failed with status '"+res.status+" "+res.statusText+"'"))
	return res.json();
}).then( obj => {
	console.log(obj.body.storage.value);
	//console.log(JSON.stringify(obj, null, "\t"));
}).catch( e => {
	console.error("Error! "+e.stack);
	Deno.exit(1);
});
