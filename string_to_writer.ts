export default async function stringToWriter(text:string, writer:Deno.Writer):Promise<void> {
    let bytes = new TextEncoder().encode(text);
    let position = 0;
    while( position < bytes.length ) {
        let wrote = await writer.write(bytes.subarray(position));
        position += wrote;
    }
}
